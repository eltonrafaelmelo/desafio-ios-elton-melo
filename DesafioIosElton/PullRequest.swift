//
//  PullRequest.swift
//  DesafioIosElton
//
//  Created by Elton Melo on 12/31/16.
//  Copyright © 2016 Elton Melo. All rights reserved.
//

import Foundation
import ObjectMapper

class PullRequest: Mappable {
    
    var id         = 0
    var title = ""
    var body = ""
    var user = User()
    var datePullRequest = ""
    var urlPage = ""
    
    required  convenience init?(_ map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        id         <- map["id"]
        title  <- map["title"]
        body              <- map["body"]
        user              <- map["user"]
        datePullRequest              <- map["created_at"]
        urlPage    <- map["html_url"]
    }
}
