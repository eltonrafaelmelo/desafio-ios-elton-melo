//
//  TOListRepositories.swift
//  DesafioIosElton
//
//  Created by Elton Melo on 12/30/16.
//  Copyright © 2016 Elton Melo. All rights reserved.
//

import Foundation
import ObjectMapper

class TOListRepositories: Mappable {
    
    var totalCount         = 0
    var incompleteResults = false
    var items      = [Item]()
    
    required  convenience init?(_ map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        totalCount         <- map["total_count"]
        incompleteResults  <- map["incomplete_results"]
        items              <- map["items"]
    }
}
