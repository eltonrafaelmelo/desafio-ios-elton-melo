//
//  TOListPullRequest.swift
//  DesafioIosElton
//
//  Created by Elton Melo on 12/30/16.
//  Copyright © 2016 Elton Melo. All rights reserved.
//

import Foundation
import ObjectMapper

//Cada item da lista deve exibir Nome / Foto do autor do PR, Título do PR, Data do PR e Body do PR


class TOListPullRequest: Mappable {
    
    var listPullRequest = [PullRequest]()
   
    required  convenience init?(_ map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        listPullRequest   <- map[" "]
    }
}
