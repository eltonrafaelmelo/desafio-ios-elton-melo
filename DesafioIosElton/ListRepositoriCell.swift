//
//  ListRepositoriCell.swift
//  DesafioIosElton
//
//  Created by Elton Melo on 12/30/16.
//  Copyright © 2016 Elton Melo. All rights reserved.
//

import UIKit

class ListRepositoriCell: UITableViewCell {

    @IBOutlet weak var nomeRepositorio: UILabel!
    @IBOutlet weak var descricao: UILabel!
    @IBOutlet weak var funk: UILabel!
    @IBOutlet weak var estrela: UILabel!
    @IBOutlet weak var nomeAutor: UILabel!
    @IBOutlet weak var imagem: UIImageView!
}
