//
//  RestClient.swift
//  DesafioIosElton
//
//  Created by Elton Melo on 12/30/16.
//  Copyright © 2016 Elton Melo. All rights reserved.
//

import Foundation
import Alamofire
import ObjectMapper
import AlamofireObjectMapper

class RestClient {
    
    class func getListRepositoriesPage(page: Int,completionHandler: (repositories: TOListRepositories?, error: ErrorType?) -> () ){
                
        let urlFinal = "https://api.github.com/search/repositories?q=language:Java&sort=stars&page=\(page)"
        
        Alamofire.request(.GET, urlFinal, parameters: nil)
            .responseObject { (response: TOListRepositories?, error: ErrorType?) in
                completionHandler(repositories: response as TOListRepositories?, error: error)
        }
    }
    
    class func getListPullRequest(fullName: String, completionHandler: (repositories: [PullRequest]?, error: ErrorType?) -> () ){
        //elastic/elasticsearch
        let urlFinal = "https://api.github.com/repos/\(fullName)/pulls"
        let url: NSURL = NSURL(string: urlFinal)!
        let request: NSURLRequest = NSURLRequest(URL: url)
        
        Alamofire.request(request).responseArray { (response: [PullRequest]?, error) in
            
            completionHandler(repositories: response , error: error)

        }
    }

}

