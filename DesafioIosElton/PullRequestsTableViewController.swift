//
//  PullRequestsTableViewController.swift
//  DesafioIosElton
//
//  Created by Elton Melo on 12/31/16.
//  Copyright © 2016 Elton Melo. All rights reserved.
//

import UIKit
import KFSwiftImageLoader

class PullRequestsTableViewController: UITableViewController {
    
    var item = Item()
    var listPullRequest = [PullRequest]()
    var util = Util.sharedInstance
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = self.item.name
        getListPull(item.fullName)
    }
    
    // MARK: - Table view data source
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return listPullRequest.count
    }
    
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("listPullRequestCell", forIndexPath: indexPath) as! ListPullRequestCell
        
        let item = self.listPullRequest[indexPath.row]
        
        cell.nameAutor.text = item.user.nomeAutor
        cell.nameDescricaoPull.text = item.body
        cell.namePullRequest.text = item.title
        
        let data = util.dateGitHub(item.datePullRequest)
        
        cell.datePull.text = data
        
        cell.imageURL.layer.cornerRadius = cell.imageURL.frame.size.width / 2
        cell.imageURL.clipsToBounds = true
        cell.imageURL.loadImageFromURLString(item.user.avatarUrl) {
            (finished, error) in

            if (!finished) {
                NSLog("\(error)")
            }
        }
        
        return cell
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
        let item = self.listPullRequest[indexPath.row]
        
        UIApplication.sharedApplication().openURL(NSURL(string: item.urlPage)!)
    }
    
    func getListPull(namePull: String){
        
        self.util.showActivityIndicator()
        
        RestClient.getListPullRequest(namePull){repositories, error in
            
            self.util.hideActivityIndicator()
            
            if let _ = error {
                
                print(error)
                
                self.util.showMessage(self, message: "\(error)")
                
            } else {
                
                self.listPullRequest = repositories!
                
                self.tableView.reloadData()
                
            }
        }
    }
}
