//
//  Item.swift
//  DesafioIosElton
//
//  Created by Elton Melo on 12/30/16.
//  Copyright © 2016 Elton Melo. All rights reserved.
//

import Foundation
import ObjectMapper

class Item: Mappable {
    
    //Cada repositório deve exibir Nome do repositório, Descrição do Repositório, Nome / Foto do autor, Número de Stars, Número de Forks
    
    var id              = 0
    var description     = ""
    var fullName        = ""
    var forksCount      = 0
    var name            = ""
    var owner           = Owner()
    var stargazersCount = 0
    
    
    required  convenience init?(_ map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        id              <- map["id"]
        description     <- map["description"]
        fullName        <- map["full_name"]
        forksCount      <- map["forks_count"]
        name            <- map["name"]
        owner           <- map["owner"]
        stargazersCount <- map["stargazers_count"]
    }
}
