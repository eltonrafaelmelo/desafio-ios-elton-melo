//
//  UtilFont.swift
//  DesafioIosElton
//
//  Created by Elton Melo on 12/31/16.
//  Copyright © 2016 Elton Melo. All rights reserved.
//

import UIKit

class UtilFont: NSObject {

    class func fontNavigationBarTitle() -> UIFont {
        return UIFont(name: "HelveticaNeue-Light", size: 17)!
    }
}
