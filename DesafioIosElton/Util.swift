//
//  Util.swift
//  DesafioIosElton
//
//  Created by Elton Melo on 12/31/16.
//  Copyright © 2016 Elton Melo. All rights reserved.
//

import UIKit
import SwiftLoader

class Util: NSObject {
    
    static let sharedInstance = Util()
    
    func showActivityIndicator(){
        showActivityIndicator("Carregando...")
    }
    
    func showActivityIndicator(title: String, color: UIColor = UIColor.whiteColor()){
        var config : SwiftLoader.Config = SwiftLoader.Config()
        config.size = 120
        config.backgroundColor = UIColor.whiteColor()
        config.spinnerColor = UIColor.grayColor()
        config.titleTextColor = UIColor.grayColor()
        config.spinnerLineWidth = 2.0
        config.cornerRadius =  30
        config.foregroundColor = UIColor.blackColor()
        config.foregroundAlpha = 0.6
        SwiftLoader.setConfig(config)
        SwiftLoader.show(title: title, animated: true)
    }
    
    func hideActivityIndicator(){
        SwiftLoader.hide()
    }
    
    func showMessage(viewController: UIViewController, message: String) {
        let alert = UIAlertController(title: "Alerta", message: "\(message)", preferredStyle: .Alert)
        alert.addAction(UIAlertAction(title: "OK", style: .Default) { _ in })
        viewController.presentViewController(alert, animated: true){}
    }
    
    func dateGitHub(gitDate :String) -> String {
        //2016-12-30T14:03:56Z
        let toArray = gitDate.componentsSeparatedByString("-")
        let aaaa = toArray[2]
        let toArray2 = aaaa.componentsSeparatedByString("T")
        var hour = toArray2[1]
        let range = hour.rangeOfString("Z")
        hour = hour.stringByReplacingCharactersInRange(range!, withString: "")
        var newDate = ""
        if (toArray.count > 2) {
            let day = toArray2[0]
            let month = toArray[1]
            let year = toArray[0]
            newDate = "\(day)/\(month)/\(year) as \(hour)"
        }
        
        return newDate
    }

}
