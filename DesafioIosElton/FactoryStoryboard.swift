//
//  FactoryStoryboard.swift
//  DesafioIosElton
//
//  Created by Elton Melo on 12/31/16.
//  Copyright © 2016 Elton Melo. All rights reserved.
//

import UIKit

class FactoryStoryboard: NSObject {

    class func storyboardMain() -> UIStoryboard {
        
        return UIStoryboard(name: "Main", bundle: nil)
    }
    
}
