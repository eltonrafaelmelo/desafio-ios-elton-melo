//
//  ListPullRequestCell.swift
//  DesafioIosElton
//
//  Created by Elton Melo on 12/31/16.
//  Copyright © 2016 Elton Melo. All rights reserved.
//

import UIKit

class ListPullRequestCell: UITableViewCell {

    @IBOutlet weak var namePullRequest: UILabel!
    @IBOutlet weak var nameDescricaoPull: UILabel!
    @IBOutlet weak var nameAutor: UILabel!
    @IBOutlet weak var datePull: UILabel!
    @IBOutlet weak var imageURL: UIImageView!
}
