//
//  HomeTableViewController.swift
//  DesafioIosElton
//
//  Created by Elton Melo on 12/30/16.
//  Copyright © 2016 Elton Melo. All rights reserved.
//

import UIKit
import KFSwiftImageLoader

class HomeTableViewController: UITableViewController {
    
    var toListRepositories = TOListRepositories()
    var util = Util.sharedInstance
    var  lastItem = Item()
    var totalContador = 34
    var contador = 1
    var listItems = [Item]()
    var getMaisItem = false
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        getList()
    }
    
    // MARK: - Table view data source
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return listItems.count
    }
    
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("listRepositoriCell", forIndexPath: indexPath) as! ListRepositoriCell
        
        let item = listItems[indexPath.row]
        
        cell.nomeAutor.text = item.owner.nomeAutor
        cell.descricao.text = item.description
        cell.funk.text = "\(item.forksCount)"
        cell.estrela.text = "\(item.stargazersCount)"
        cell.nomeRepositorio.text = item.name
        
        cell.imagem.layer.cornerRadius = cell.imagem.frame.size.width / 2
        cell.imagem.clipsToBounds = true
        cell.imagem.loadImageFromURLString(item.owner.avatarUrl) {
            (finished, error) in
            //                self.hideActivityIndicator()
            if (!finished) {
                NSLog("\(error)")
            }
        }
        
        return cell
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
        let item = listItems[indexPath.row]
        
        let screen = FactoryStoryboard.storyboardMain().instantiateViewControllerWithIdentifier("pullRequestsTableViewController") as! PullRequestsTableViewController
        screen.item = item
        
        navigationController?.pushViewController(screen, animated: true)
    }
    
    //getMoviePage willDisplayCell
    override func tableView(tableView: UITableView, willDisplayCell cell: UITableViewCell, forRowAtIndexPath indexPath: NSIndexPath) {
        
        let item = listItems[indexPath.row]
        
        if contador != totalContador {
            
            if lastItem.name.isEqual(item.name){
                print("Ultimo filme da lista: \(item.name)")
                lastItem = Item()
                getList()
            }
        }
    }
    
    
    // MARK: - Get List repositories
    func getList(){
        
        self.util.showActivityIndicator()
        
        RestClient.getListRepositoriesPage(contador) {repositories, error in
            
            self.util.hideActivityIndicator()
            
            if let _ = error {
                
                print(error)
                
                self.util.showMessage(self, message: "\(error)")
                
            } else {
                
                self.toListRepositories = repositories!
                
                let list = repositories!
                
                print("Numero de items: \(list.items.count)")
                
                self.contador = self.contador + 1
                
                self.addMoreMovie(list.items)
                
            }
        }
    }
    
    func addMoreMovie(listT :[Item]) {
        
        for itemReceive in listT {
            
            listItems.append(itemReceive)
        }
        
        print("\(listItems.count)")
        
        if listItems.count > 0 {
            
            lastItem = listItems.last!
            let teste = listItems.last!
            print(teste.name)
            print(lastItem.name)
            
        }
        
        
        
        self.tableView.reloadData()
        
    }
    
    
}
