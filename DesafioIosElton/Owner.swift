//
//  Owner.swift
//  DesafioIosElton
//
//  Created by Elton Melo on 12/30/16.
//  Copyright © 2016 Elton Melo. All rights reserved.
//

import Foundation
import ObjectMapper

class Owner: Mappable {
    
    var avatarUrl = ""
    var nomeAutor = ""
    
    required  convenience init?(_ map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        avatarUrl  <- map["avatar_url"]
        nomeAutor  <- map["login"]
    }
}

